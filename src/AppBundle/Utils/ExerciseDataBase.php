<?php

namespace AppBundle\Utils;

abstract class ExerciseDataBase
{
    protected $result = [];

    public function getResult()
    {
        return $this->result;
    }

    protected $error = null;

    public function getError()
    {
        return $this->error;
    }

    public abstract function calculateResult();

    public function calculate() {
        try {
            $this->setErrorHandler();
            $this->calculateResult();
        } catch (CalculateException $e) {
            $this->result = [];
            $this->error = $e->getMessage();
        } finally {
            $this->restoreErrorHandler();
        }
    }

    public function setErrorHandler()
    {
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            switch ($errstr) {
                case 'Division by zero':
                    throw new CalculateException('Попытка деления на ноль.');
                default:
                    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        });
    }

    public function restoreErrorHandler()
    {
        restore_error_handler();
    }
}
