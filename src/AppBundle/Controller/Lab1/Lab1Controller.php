<?php

namespace AppBundle\Controller\Lab1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab1")
 */
class Lab1Controller extends Controller
{
    /**
     * @Route("/", name="lab1_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('lab1/index.html.twig');
    }

    /**
     * @Route("/about/", name="lab1_about")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('lab1/about.html.twig');
    }

    /**
     * @Route("/contacts/", name="lab1_contacts")
     */
    public function contactsAction(Request $request)
    {
        return $this->render('lab1/contacts.html.twig');
    }

    /**
     * @Route("/category/{name}/", name="lab1_category")
     */
    public function categoryAction(Request $request, $name)
    {
        $categories = ['alpine-skiing', 'cross-country-skiing', 'snowboarding'];
        if (!in_array($name, $categories))
            throw $this->createNotFoundException('Неправильная категория товаров.');

        return $this->render(sprintf('lab1/category/%s.html.twig', $name));
    }
}
