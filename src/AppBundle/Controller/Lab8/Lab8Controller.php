<?php

namespace AppBundle\Controller\Lab8;

use AppBundle\Entity\Lab8\Appointment;
use AppBundle\Entity\Lab8\Doctor;
use AppBundle\Entity\Lab8\Patient;

use Doctrine\ORM\NoResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab8")
 */
class Lab8Controller extends Controller
{
    /**
     * @Route("/", name="lab8_index")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute('lab8_doctors_list');
    }

    /**
     * @Route("/doctors/", name="lab8_doctors_list")
     */
    public function doctorsListAction(Request $request)
    {
        $DoctorClass = Doctor::class;
        $manager = $this->getDoctrine()->getManager();

        $doctorsList = $manager->createQuery("
            SELECT doctor, specialty FROM $DoctorClass doctor
            JOIN doctor.specialty specialty ORDER BY doctor.id ASC
        ")->getResult();

        return $this->render('lab8/doctors_list.html.twig', [
            'doctors_list' => $doctorsList,
        ]);
    }

    /**
     * @Route("/doctors/{id}/", name="lab8_doctor_view")
     */
    public function doctorViewAction(Request $request, $id)
    {
        $DoctorClass = Doctor::class;
        $AppointmentClass = Appointment::class;
        $manager = $this->getDoctrine()->getManager();

        try {
            $doctor = $manager->createQuery("
                SELECT doctor, specialty FROM $DoctorClass doctor
                JOIN doctor.specialty specialty WHERE doctor.id = :id
                ORDER BY doctor.id ASC
            ")->setParameter('id', $id)->getSingleResult();
        } catch (NoResultException $e) {
            throw $this->createNotFoundException('Данный врач не существует.');
        }

        $appointmentsList = $manager->createQuery("
            SELECT appointment, patient FROM $AppointmentClass appointment
            JOIN appointment.patient patient JOIN appointment.doctor doctor
            WHERE doctor.id = :id ORDER BY appointment.dateTime ASC
        ")->setParameter('id', $id)->getResult();

        return $this->render('lab8/doctor_view.html.twig', [
            'doctor' => $doctor,
            'appointments_list' => $appointmentsList,
        ]);
    }

    /**
     * @Route("/patients/", name="lab8_patients_list")
     */
    public function patientsListAction(Request $request)
    {
        $PatientClass = Patient::class;
        $manager = $this->getDoctrine()->getManager();

        $patientsList = $manager->createQuery(
            "SELECT patient FROM $PatientClass patient ORDER BY patient.id ASC"
        )->getResult();

        return $this->render('lab8/patients_list.html.twig', [
            'patients_list' => $patientsList,
        ]);
    }

    /**
     * @Route("/patients/{id}/", name="lab8_patient_view")
     */
    public function patientViewAction(Request $request, $id)
    {
        $PatientClass = Patient::class;
        $AppointmentClass = Appointment::class;
        $manager = $this->getDoctrine()->getManager();

        try {
            $patient = $manager->createQuery("
                SELECT patient FROM $PatientClass patient
                WHERE patient.id = :id ORDER BY patient.id ASC
            ")->setParameter('id', $id)->getSingleResult();
        } catch (NoResultException $e) {
            throw $this->createNotFoundException('Данный пациент не существует.');
        }

        $appointmentsList = $manager->createQuery("
            SELECT appointment, doctor FROM $AppointmentClass appointment
            JOIN appointment.doctor doctor JOIN appointment.patient patient
            WHERE patient.id = :id ORDER BY appointment.dateTime ASC
        ")->setParameter('id', $id)->getResult();

        return $this->render('lab8/patient_view.html.twig', [
            'patient' => $patient,
            'appointments_list' => $appointmentsList,
        ]);
    }
}
