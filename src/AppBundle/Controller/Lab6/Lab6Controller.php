<?php

namespace AppBundle\Controller\Lab6;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab6")
 */
class Lab6Controller extends Controller
{
    /**
     * @Route("/", name="lab6_index")
     */
    public function indexAction(Request $request)
    {
        // Задание 1
        $products = [
            'Яблоко' => 10,
            'Апельсин' => 15,
            'Мандарин' => 13,
        ];

        $products['Лимон'] = 20;
        $products['Груша'] = 17;

        $productsCount = count($products);
        $totalPrice = array_sum($products);

        ksort($products);

        // Задание 2
        $genSymbolsTable = function ($source) {
            $start = (int)((strlen($source) - 1) / 2);
            $length = strlen($source) - 2 * $start;
            for (; $length < strlen($source); $start -= 1, $length += 2)
                yield substr($source, $start, $length);
            for (; $length > 0; $start += 1, $length -= 2)
                yield substr($source, $start, $length);
        };
        $symbolsTable = $genSymbolsTable('DCBABCA');

        return $this->render('lab6/index.html.twig', [
            'products' => $products,
            'products_count' => $productsCount,
            'total_price' => $totalPrice,
            'symbols_table' => $symbolsTable,
        ]);
    }
}
