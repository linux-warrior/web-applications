<?php

namespace AppBundle\Controller\Lab5;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab5")
 */
class Lab5Controller extends Controller
{
    const EXERCISE_IDS = [1, 2, 3, 4, 6];

    /**
     * @Route("/", name="lab5_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('lab5/index.html.twig', [
            'EXERCISE_IDS' => self::EXERCISE_IDS
        ]);
    }

    /**
     * @Route("/exercise{exerciseId}/", requirements={"exerciseId": "\d+"}, name="lab5_exercise")
     */
    public function exerciseAction(Request $request, $exerciseId)
    {
        if (!in_array($exerciseId, self::EXERCISE_IDS))
            throw $this->createNotFoundException('Неправильный номер задания.');

        $ExerciseData = sprintf('AppBundle\Entity\Lab5\Exercise%sData', $exerciseId);
        $ExerciseType = sprintf('AppBundle\Form\Lab5\Exercise%sType', $exerciseId);

        $data = new $ExerciseData();
        $form = $this->createForm($ExerciseType, $data);
        $form->handleRequest($request);

        if ($form->isValid())
            $data->calculate();

        return $this->render('lab5/exercise.html.twig', [
            'EXERCISE_IDS' => self::EXERCISE_IDS,
            'exercise_id' => $exerciseId,
            'form' => $form->createView(),
            'result' => $data->getResult(),
            'error' => $data->getError(),
        ]);
    }
}
