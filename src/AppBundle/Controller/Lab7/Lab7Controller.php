<?php

namespace AppBundle\Controller\Lab7;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab7")
 */
class Lab7Controller extends Controller
{
    /**
     * @Route("/", name="lab7_index")
     */
    public function indexAction(Request $request)
    {
        $dataDir = realpath($this->getParameter('kernel.root_dir') . '/../var/data');
        $filePath = $dataDir . '/lab7/autumn.txt';

        $fileExists = file_exists($filePath);
        $isReadable = is_readable($filePath);
        $isWritable = is_writable($filePath);

        $fileSize = @filesize($filePath);
        if ($fileSize === false)
            $fileSize = null;

        // Загружаем содержимое файла в массив.
        $errors = [];
        $fileContents = [];

        $handle = null;
        try {
            $handle = @fopen($filePath, 'rb');
            if (!$handle)
                throw new \RuntimeException("Не удалось открыть файл $filePath.");

            while (($buffer = fgets($handle, 4096)) !== false)
                $fileContents[] = $buffer;
        } catch (\RuntimeException $e) {
            $errors[] = $e->getMessage();
        } finally {
            if ($handle)
                fclose($handle);
        }

        if ($fileContents) {
            // Сохраняем результат в новый файл.
            $pathParts = pathinfo($filePath);
            $newFileDir = "{$pathParts['dirname']}/modified";
            $newFilePath = "{$newFileDir}/{$pathParts['basename']}";

            $handle = null;
            try {
                if (!is_dir($newFileDir) and !@mkdir($newFileDir, 0755, true))
                    throw new \RuntimeException("Ошибка при создании директории $newFileDir.");

                $handle = @fopen($newFilePath, 'wb');
                if (!$handle)
                    throw new \RuntimeException("Не удалось открыть файл $newFilePath.");
                if (!flock($handle, LOCK_EX))
                    throw new \RuntimeException("Не удалось заблокировать файл $newFilePath для записи.");

                // Удаляем последнюю строку содержимого файла.
                foreach ($fileContents as $i => $line)
                    if ($i !== count($fileContents) - 1)
                        fwrite($handle, $line);

                fflush($handle);
                flock($handle, LOCK_UN);
            } catch (\RuntimeException $e) {
                $errors[] = $e->getMessage();
            } finally {
                if ($handle)
                    fclose($handle);
            }
        }

        return $this->render('lab7/index.html.twig', [
            'file_path' => $filePath,
            'errors' => $errors,
            'file_exists' => $fileExists,
            'is_readable' => $isReadable,
            'is_writable' => $isWritable,
            'file_size' => $fileSize,
            'file_contents' => $fileContents,
        ]);
    }
}
