<?php

namespace AppBundle\Controller\Lab3;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lab3")
 */
class Lab3Controller extends Controller
{
    /**
     * @Route("/", name="lab3_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('lab3/index.html.twig');
    }

    /**
     * @Route("/feedback/", name="lab3_feedback")
     */
    public function feedbackAction(Request $request)
    {
        if ($request->isMethod('POST'))
            return $this->redirectToRoute('lab3_index');

        return $this->render('lab3/feedback.html.twig');
    }
}
