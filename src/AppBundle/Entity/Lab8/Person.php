<?php

namespace AppBundle\Entity\Lab8;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(columns={"first_name", "last_name"})
 *     }
 * )
 */
abstract class Person
{
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
    protected $lastName;

    /**
     * @ORM\Column(type="text")
     */
    protected $description = '';

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    function __toString()
    {
        return "{$this->firstName} {$this->lastName}";
    }
}
