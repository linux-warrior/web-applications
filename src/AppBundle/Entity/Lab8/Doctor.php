<?php

namespace AppBundle\Entity\Lab8;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Doctor extends Person
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Specialty")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $specialty;

    public function getId()
    {
        return $this->id;
    }

    public function getSpecialty()
    {
        return $this->specialty;
    }

    public function setSpecialty(Specialty $specialty)
    {
        $this->specialty = $specialty;

        return $this;
    }
}
