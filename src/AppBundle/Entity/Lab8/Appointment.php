<?php

namespace AppBundle\Entity\Lab8;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(columns={"doctor_id", "date_time"}),
 *         @ORM\UniqueConstraint(columns={"patient_id", "date_time"}),
 *     },
 *     indexes={
 *         @ORM\Index(columns={"date_time"})
 *     }
 * )
 */
class Appointment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Doctor")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(targetEntity="Patient")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $patient;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    protected $dateTime;

    public function getId()
    {
        return $this->id;
    }

    public function getDateTime()
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTime $dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getDoctor()
    {
        return $this->doctor;
    }

    public function setDoctor(Doctor $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getPatient()
    {
        return $this->patient;
    }

    public function setPatient(Patient $patient)
    {
        $this->patient = $patient;

        return $this;
    }
}
