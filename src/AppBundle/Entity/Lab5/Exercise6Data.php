<?php

namespace AppBundle\Entity\Lab5;

use AppBundle\Utils\CalculateException;
use AppBundle\Utils\ExerciseDataBase;
use Symfony\Component\Validator\Constraints as Assert;

class Exercise6Data extends ExerciseDataBase
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("array")
     */
    public $x = [];

    public function calculateResult()
    {
        for ($i = 0; $i < count($this->x); $i++) {
            if ($this->x[$i] < 0)
                break;
        }
        if ($i < count($this->x))
            $this->result['Индекс первого отрицательного элемента'] = $i + 1;
        else
            throw new CalculateException('Отрицательный элемент не найден.');

        for ($i = count($this->x) - 1; $i >= 0; $i--) {
            if ($this->x[$i] < 0)
                break;
        }
        $this->result['Индекс последнего отрицательного элемента'] = $i + 1;
    }
}
