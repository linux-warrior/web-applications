<?php

namespace AppBundle\Entity\Lab5;

use AppBundle\Utils\ExerciseDataBase;
use Symfony\Component\Validator\Constraints as Assert;

class Exercise1Data extends ExerciseDataBase
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $a;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $b;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $k;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $m;

    public function calculateResult() {
        $this->result['C'] = sqrt(($this->a - $this->b) ** 2 / abs($this->k - $this->m));
        $this->result['A'] =
            sin(M_PI / 6) * $this->result['C'] ** 2 -
            $this->result['C'] * ($this->a - $this->b) / ($this->a * $this->b * $this->k);
    }
}
