<?php

namespace AppBundle\Entity\Lab5;

use AppBundle\Utils\ExerciseDataBase;
use Symfony\Component\Validator\Constraints as Assert;

class Exercise2Data extends ExerciseDataBase
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $alpha;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $beta;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $gamma;

    public function calculateResult()
    {
        $this->result['k'] =
            $this->alpha ** 2 * $this->beta ** 2 -
            $this->alpha * ($this->beta - $this->gamma) ** 2;
        if ($this->result['k'] >= 2) {
            $this->result['y1'] = ($this->alpha + $this->beta) * $this->result['k'];
            $this->result['y2'] = ($this->alpha - $this->beta) ** 2;
        } else {
            $this->result['y1'] = $this->alpha ** 2;
            $this->result['y2'] =
                $this->result['k'] ** 2 /
                ($this->alpha * $this->beta * $this->gamma);
        }
    }
}
