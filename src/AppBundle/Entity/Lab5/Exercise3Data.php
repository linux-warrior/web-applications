<?php

namespace AppBundle\Entity\Lab5;

use AppBundle\Utils\ExerciseDataBase;
use Symfony\Component\Validator\Constraints as Assert;

class Exercise3Data extends ExerciseDataBase
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $haystack = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $needle = '';

    public function calculateResult()
    {
        $this->result['Количество результатов поиска'] = substr_count($this->haystack, $this->needle);
    }
}
