<?php

namespace AppBundle\Entity\Lab5;

use AppBundle\Utils\ExerciseDataBase;
use Symfony\Component\Validator\Constraints as Assert;

class Exercise4Data extends ExerciseDataBase
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("array")
     */
    public $x = [];

    /**
     * @Assert\NotBlank()
     * @Assert\Type("array")
     */
    public $l = [];

    /**
     * @Assert\IsTrue(message = "Массивы x и l должны быть одинаковой длины.")
     */
    public function hasXAndLEqualSizes()
    {
        return count($this->x) === count($this->l);
    }

    public function calculateResult() {
        $z = [];
        $sumsDiff = array_sum($this->x) - array_sum($this->l);
        for ($i = 0; $i < count($this->x); $i++)
            $z[] = $sumsDiff / sqrt(abs($this->x[$i] * $this->l[$i]));

        $this->result['z'] = implode(', ', $z);
    }
}
