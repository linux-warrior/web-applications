<?php

namespace AppBundle\Entity;

class CollectionEntry
{
    public $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }
}
