<?php

namespace AppBundle\Form\Lab5;

use AppBundle\Entity\Lab5\Exercise1Data;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Exercise1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach (['a', 'b', 'k', 'm'] as $fieldName)
            $builder->add($fieldName, NumberType::class, ['label' => $fieldName]);
        $builder
            ->add('submit', SubmitType::class, ['label' => 'Отправить'])
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exercise1Data::class,
            'csrf_protection' => false,
        ]);
    }
}
