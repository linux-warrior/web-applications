<?php

namespace AppBundle\Form\Lab5;

use AppBundle\Entity\Lab5\Exercise4Data;
use AppBundle\Form\Type\CollectionEntryType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Exercise4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach (['x', 'l'] as $fieldName) {
            $builder->add($fieldName, CollectionType::class, [
                'label' => $fieldName,
                'entry_type' => CollectionEntryType::class,
                'entry_options' => [
                    'value_type' => NumberType::class,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
            ]);
            $builder->get($fieldName)->addModelTransformer(new CallbackTransformer(
                function ($original) {
                    return $original;
                },
                function ($submitted) {
                    return array_values($submitted);
                }
            ));
        }

        $builder
            ->add('submit', SubmitType::class, ['label' => 'Отправить'])
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exercise4Data::class,
            'csrf_protection' => false,
        ]);
    }
}
