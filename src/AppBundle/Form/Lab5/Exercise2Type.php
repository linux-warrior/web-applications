<?php

namespace AppBundle\Form\Lab5;

use AppBundle\Entity\Lab5\Exercise2Data;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Exercise2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach (['alpha', 'beta', 'gamma'] as $fieldName)
            $builder->add($fieldName, NumberType::class, ['label' => $fieldName]);
        $builder
            ->add('submit', SubmitType::class, ['label' => 'Отправить'])
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exercise2Data::class,
            'csrf_protection' => false,
        ]);
    }
}
