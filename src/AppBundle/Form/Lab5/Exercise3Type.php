<?php

namespace AppBundle\Form\Lab5;

use AppBundle\Entity\Lab5\Exercise3Data;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Exercise3Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('haystack', TextType::class, ['label' => 'Строка'])
            ->add('needle', TextType::class, ['label' => 'Подстрока'])
            ->add('submit', SubmitType::class, ['label' => 'Отправить'])
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exercise3Data::class,
            'csrf_protection' => false,
        ]);
    }
}
