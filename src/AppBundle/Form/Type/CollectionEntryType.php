<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\CollectionEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', $options['value_type'], [
                'label' => $options['value_label'],
            ])
            ->addModelTransformer(new CallbackTransformer(
                function ($original) {
                    return new CollectionEntry($original);
                },
                function (CollectionEntry $submitted) {
                    return $submitted->value;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CollectionEntry::class,
            'value_type' => TextType::class,
            'value_label' => false,
        ]);
    }
}
