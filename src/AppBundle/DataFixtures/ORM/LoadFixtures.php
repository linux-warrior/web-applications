<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Lab8\Appointment;
use AppBundle\Entity\Lab8\Doctor;
use AppBundle\Entity\Lab8\Patient;
use AppBundle\Entity\Lab8\Specialty;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFixtures extends AbstractFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->loadSpecialties($manager);
        $this->loadDoctors($manager);
        $this->loadPatients($manager);
        $this->loadAppointments($manager);
    }

    public function loadSpecialties(ObjectManager $manager)
    {
        $specialtiesDict = [
            'infections' => (new Specialty())
                ->setName('Инфекционист'),
            'endocrinology' => (new Specialty())
                ->setName('Эндокринолог'),
            'oncology' => (new Specialty())
                ->setName('Онколог'),
            'neurology' => (new Specialty())
                ->setName('Невролог'),
            'surgery' => (new Specialty())
                ->setName('Хирург'),
            'immunology' => (new Specialty())
                ->setName('Иммунолог'),
        ];

        foreach ($specialtiesDict as $code => $specialty) {
            $this->addReference("specialty-$code", $specialty);
            $manager->persist($specialty);
        }

        $manager->flush();
    }

    public function loadDoctors(ObjectManager $manager)
    {
        $doctorsDict = [
            'house' => (new Doctor())
                ->setFirstName('Грегори')
                ->setLastName('Хаус')
                ->setSpecialty($this->getReference('specialty-infections')),
            'cuddy' => (new Doctor())
                ->setFirstName('Лиза')
                ->setLastName('Кадди')
                ->setSpecialty($this->getReference('specialty-endocrinology')),
            'wilson' => (new Doctor())
                ->setFirstName('Джеймс')
                ->setLastName('Уилсон')
                ->setSpecialty($this->getReference('specialty-oncology')),
            'foreman' => (new Doctor())
                ->setFirstName('Эрик')
                ->setLastName('Форман')
                ->setSpecialty($this->getReference('specialty-neurology')),
            'chase' => (new Doctor())
                ->setFirstName('Роберт')
                ->setLastName('Чейз')
                ->setSpecialty($this->getReference('specialty-surgery')),
            'cameron' => (new Doctor())
                ->setFirstName('Эллисон')
                ->setLastName('Кэмерон')
                ->setSpecialty($this->getReference('specialty-immunology')),
        ];

        foreach ($doctorsDict as $code => $doctor) {
            $this->addReference("doctor-$code", $doctor);
            $manager->persist($doctor);
        }

        $manager->flush();
    }

    public function loadPatients(ObjectManager $manager)
    {
        $patientsDict = [
            'frodo' => (new Patient())
                ->setFirstName('Фродо')
                ->setLastName('Бэггинс')
                ->setPhone('111111')
                ->setAddress('Шир'),
            'sam' => (new Patient())
                ->setFirstName('Сэмуайз')
                ->setLastName('Гэмджи')
                ->setPhone('222222')
                ->setAddress('Шир'),
            'merry' => (new Patient())
                ->setFirstName('Мериадок')
                ->setLastName('Брендибак')
                ->setPhone('333333')
                ->setAddress('Шир'),
            'pippin' => (new Patient())
                ->setFirstName('Перегрин')
                ->setLastName('Тук')
                ->setPhone('444444')
                ->setAddress('Шир'),
            'gandalf' => (new Patient())
                ->setFirstName('Гэндальф')
                ->setLastName('Серый'),
            'aragorn' => (new Patient())
                ->setFirstName('Арагорн')
                ->setLastName('сын Араторна')
                ->setAddress('Гондор'),
        ];

        foreach ($patientsDict as $code => $patient) {
            $this->addReference("patient-$code", $patient);
            $manager->persist($patient);
        }

        $manager->flush();
    }

    public function loadAppointments(ObjectManager $manager)
    {
        $appointmentsList = [
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-01 9:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-sam'))
                ->setDateTime(new \DateTime('2015-06-01 9:30')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-merry'))
                ->setDateTime(new \DateTime('2015-06-01 10:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-pippin'))
                ->setDateTime(new \DateTime('2015-06-02 9:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-gandalf'))
                ->setDateTime(new \DateTime('2015-06-02 9:30')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-house'))
                ->setPatient($this->getReference('patient-aragorn'))
                ->setDateTime(new \DateTime('2015-06-02 10:00')),

            (new Appointment())
                ->setDoctor($this->getReference('doctor-cuddy'))
                ->setPatient($this->getReference('patient-gandalf'))
                ->setDateTime(new \DateTime('2015-06-03 11:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-cuddy'))
                ->setPatient($this->getReference('patient-pippin'))
                ->setDateTime(new \DateTime('2015-06-04 11:00')),

            (new Appointment())
                ->setDoctor($this->getReference('doctor-foreman'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-01 10:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-foreman'))
                ->setPatient($this->getReference('patient-sam'))
                ->setDateTime(new \DateTime('2015-06-01 10:30')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-foreman'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-05 10:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-foreman'))
                ->setPatient($this->getReference('patient-sam'))
                ->setDateTime(new \DateTime('2015-06-05 10:30')),

            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-01 11:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-sam'))
                ->setDateTime(new \DateTime('2015-06-01 12:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-aragorn'))
                ->setDateTime(new \DateTime('2015-06-01 13:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-02 11:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-04 11:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-aragorn'))
                ->setDateTime(new \DateTime('2015-06-04 13:00')),
            (new Appointment())
                ->setDoctor($this->getReference('doctor-chase'))
                ->setPatient($this->getReference('patient-sam'))
                ->setDateTime(new \DateTime('2015-06-05 12:00')),

            (new Appointment())
                ->setDoctor($this->getReference('doctor-cameron'))
                ->setPatient($this->getReference('patient-frodo'))
                ->setDateTime(new \DateTime('2015-06-01 12:00')),
        ];

        foreach ($appointmentsList as $appointment) {
            $manager->persist($appointment);
        }

        $manager->flush();
    }
}
