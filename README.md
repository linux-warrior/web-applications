Основы разработки веб-приложений
================================

Лабораторные работы по дисциплине «Основы разработки веб-приложений». Выполнены
в виде одного сайта на PHP + [Symfony](http://symfony.com/).

Системные требования
--------------------

* PHP 5.6 и выше
* PDO для SQLite
* [Зависимости Symfony](http://symfony.com/doc/current/reference/requirements.html)
* [Composer](https://getcomposer.org/)

Установка
---------

Загружаем зависимости:

```
$ composer install --no-interaction
```

Создаем базу данных для приложения:

```
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force
```

Загружаем тестовые данные в базу:

```
$ php bin/console doctrine:fixtures:load
```

Запускаем веб-сервер для разработчиков:

```
$ php bin/console server:run
```

После этого веб-приложение будет доступно по адресу: <http://localhost:8000/>.
