(function($) {
    $.fn.extend({
        activateNavs: function() {
            var $this = $(this);

            $this.each(function() {
                var $nav = $(this);
                $nav.children('li').each(function() {
                    var $item = $(this);
                    var link = $item.children('a').attr('href');
                    if (link === document.location.pathname) {
                        $item.addClass('active');
                        return false;
                    }
                });
            });

            return $this;
        },

        collectionWidget: function() {
            function addDeleteControl($itemForm) {
                var $deleteControl = $(
                    '<a class="btn btn-sm btn-danger collection-widget-delete" href="#" ' +
                            'role="button" aria-label="Удалить">' +
                        '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
                    '</a>'
                );
                $itemForm.append($deleteControl);
            }

            function addItemForm($collectionWidget, $createControl) {
                var formIndex = $collectionWidget.data('index');
                $collectionWidget.data('index', formIndex + 1);
                var formPrototype = $collectionWidget.data('prototype');
                formPrototype = formPrototype.replace(/__name__label__/g, formIndex);
                formPrototype = formPrototype.replace(/__name__/g, formIndex);
                var $newForm = $(formPrototype);
                addDeleteControl($newForm);
                $createControl.before($newForm);
            }

            var $this = $(this);

            $this.each(function() {
                var $collectionWidget = $(this);
                var $itemsFormsList = $collectionWidget.children('.form-group');

                var lastFormIndex = 0;
                if ($itemsFormsList.length) {
                    // Находим в последней форме одно из полей, у которого задано имя
                    var $lastInput = $itemsFormsList.last().find(':input[name]');
                    // Определяем по имени этого поля номер последней формы
                    lastFormIndex = $lastInput.attr('name').match(/\[(\d+)\]$/)[1];
                    lastFormIndex = (lastFormIndex !== undefined) ? Number(lastFormIndex) : 0;
                }
                $collectionWidget.data('index', lastFormIndex + 1);

                $itemsFormsList.each(function() {
                    addDeleteControl($(this));
                });
                var $createControl = $(
                    '<div class="form-group">' +
                        '<a class="btn btn-success collection-widget-create" href="#" role="button">' +
                            'Добавить' +
                        '</a>' +
                    '</div>'
                );
                $collectionWidget.append($createControl);

                $collectionWidget.on('click', '.collection-widget-delete', function(e) {
                    e.preventDefault();
                    $(this).closest('.form-group').remove();
                }).on('click', '.collection-widget-create', function(e) {
                    e.preventDefault();
                    addItemForm($collectionWidget, $createControl);
                });
            });

            return $this;
        }
    });
})(jQuery);
